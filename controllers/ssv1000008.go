//Version: v0.0.1
package controllers

import (
	"git.forms.io/isaving/sv/ssv1000008/models"
	"git.forms.io/isaving/sv/ssv1000008/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv1000008Controller struct {
    controllers.CommController
}

func (*Ssv1000008Controller) ControllerName() string {
	return "Ssv1000008Controller"
}

// @Desc Ssv1000008Controller Controller
// @Description Entry
// @Param Ssv1000009 body models.SSV1000008I true "body for model content"
// @Success 200 {object} models.SSV1000008O
// @router /SV100008 [post]
// @Date 2020-12-10
func (c *Ssv1000008Controller) Ssv1000008() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv1000008Controller.Ssv1000008 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv1000008I := &models.SSV1000008I{}
	if err :=  models.UnPackRequest(c.Req.Body, ssv1000008I); err != nil {
		c.SetServiceError(err)
		return
	}
  if err := ssv1000008I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv1000008 := &services.Ssv1000008Impl{} 
    ssv1000008.New(c.CommController)
	ssv1000008.Sv100008I = ssv1000008I

	ssv1000008O, err := ssv1000008.Ssv1000008(ssv1000008I)

	if err != nil {
		log.Errorf("Ssv1000008Controller.Ssv1000008 failed, err=%v", errors.ServiceErrorToString(err))
		c.SetServiceError(err)
		return
	}
	responseBody, err := ssv1000008O.PackResponse()
	if err != nil {
		c.SetServiceError(err)
		return
	}
	c.SetAppBody(responseBody)
}
// @Title Ssv1000008 Controller
// @Description ssv1000008 controller
// @Param Ssv1000008 body models.SSV1000008I true body for SSV1000008 content
// @Success 200 {object} models.SSV1000008O
// @router /create [post]
func (c *Ssv1000008Controller) SWSsv1000008() {
	//Here is to generate API documentation, no need to implement methods
}

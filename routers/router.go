////Version: v0.0.1
package routers

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/isaving/sv/ssv1000008/controllers"
	"git.forms.io/universe/common/event_handler/register"
	"github.com/astaxie/beego"
)

// @Desc Select the appropriate transaction route according to TOPIC
// @Author
// @Date 2020-12-04
func InitRouter() error {

	eventRouteReg := register.NewEventHandlerRegister()

	bc := beego.AppConfig

	eventRouteReg.Router(bc.DefaultString(constant.TopicPrefix + "ssv1000008","SV100008"),
		&controllers.Ssv1000008Controller{}, "Ssv1000008")

	return nil
}

// @Desc transaction router for swagger
// @Author
// @Date 2020-12-04
func init() {

	ns := beego.NewNamespace("isaving/v1/ssv1000008",
		beego.NSNamespace("/ssv1000008",
			beego.NSInclude(
				&controllers.Ssv1000008Controller{},
			),
		),
	)
	beego.AddNamespace(ns)
}

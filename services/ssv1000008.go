//Version: v0.0.1
package services

import (
	constants "git.forms.io/isaving/sv/ssv1000008/constant"
	"git.forms.io/isaving/sv/ssv1000008/models"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/services"
	"git.forms.io/universe/solapp-sdk/log"
)
type Ssv1000008Impl struct {
    services.CommonService
	//TODO ADD Service Self Define Field
    Sv100008O         *models.SSV1000008O
    Sv100008I         *models.SSV1000008I
}
// @Desc Ssv1000008 process
// @Author
// @Date 2020-12-04
func (impl *Ssv1000008Impl) Ssv1000008(ssv1000008I *models.SSV1000008I) (ssv1000008O *models.SSV1000008O, err error) {

	impl.Sv100008I = ssv1000008I
	//AC000001 --查询客户状态
	if err :=impl.CU000007(); nil != err {
		return nil, err
	}

	ssv1000008O = &models.SSV1000008O{
		ContractCount: impl.Sv100008O.ContractCount,
	}

	return ssv1000008O, nil
}

func (impl *Ssv1000008Impl) CU000007() error {
	rspBody, err :=impl.Sv100008I.PackRequest()
	if nil != err {
		return errors.New(err, constants.ERRCODE1)
	}
	//AC000001 --新增核算账户信息
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_ID_COMMON, constants.CU000007, rspBody);
	if err != nil{
		log.Errorf("CU000007, err:%v",err)
		return err
	}
	Sv100008O := &models.SSV1000008O{}
	if err := Sv100008O.UnPackResponse(resBody); nil != err {
		return errors.New(err, constants.ERRCODE2)
	}
	impl.Sv100008O = Sv100008O
	return nil
}